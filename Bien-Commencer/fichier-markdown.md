# Partie 1

## Sous-partie 1.1
**Texte en gras** dans cette partie

## Sous-partie 1.2
*Texte en italique* dans cette partie

## Sous-partie 1.3
Lien hypertexte [Pix](https://pix.fr/) pour aller travailler ses compétences numériques

## Sous-partie 1.4
Ecriture d'une liste à puce :
- Item 1
- Item 2
- Item 3

ou

* Item 1
* Item 2
* Item 3

## Sous-partie 1.5
Ecriture d'une liste numérotée :
1. Item 1
2. Item 2
3. Item 3

## Sous-partie 1.6
```python
    def foo(n):
        return n**2
